import time
from queue import Queue

import numpy as np
import cv2
from PyQt5.QtCore import QObject, pyqtSignal

from model.dicom_model import DicomModel


class ComponentesConexas(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)
    tiempo = pyqtSignal(float)

    def __init__(self, modelo: DicomModel, x, y, color, parent=None):
        super().__init__(parent)
        self.__modelo = modelo
        self.__cortes = modelo.get_all_cortes_pixels()
        self.__x = x
        self.__y = y
        self.__corte = self.__modelo.get_index_corte_actual()
        self.__valor = self.__cortes[self.__corte][y, x]
        self.__color = color
        self._isRunning = True

    def stop(self):
        self._isRunning = False

    def run(self):
        inicio = time.monotonic()
        etiquetas = []
        height, width = self.__cortes[0].shape
        mascara_binaria = np.zeros((height, width, len(self.__cortes)), dtype=np.bool)
        # Obtiene las componentes conexas de cada uno de los cortes mediante la umbralización
        for i, imagen in enumerate(self.__cortes):
            binary = imagen >= self.__valor
            mascara = np.zeros_like(binary, dtype=np.uint8)
            mascara[binary] = 255
            ret_val, labels = cv2.connectedComponents(mascara)
            etiquetas.append(labels)
            if not self._isRunning:
                break

        cola = Queue()
        if self._isRunning:
            # Obtiene la etiqueta del punto seleccionado y los puntos que forman esa region
            mascara_binaria[:, :, self.__corte] = etiquetas[self.__corte] == etiquetas[self.__corte][self.__y, self.__x]
            puntos = np.where(mascara_binaria[:, :, self.__corte])

            # Como hay que transferir esos puntos a las regiones inferiores y superiores se realiza mediante una cola
            if self.__corte > 0:
                cola.put((self.__corte-1, True, tuple(zip(*puntos))))
            if self.__corte < len(self.__cortes)-1:
                cola.put((self.__corte+1, False, tuple(zip(*puntos))))

        # Mientras en la cola haya cortes por procesar se procesan
        while not cola.empty() and self._isRunning:
            # Se obtiene la cabeza de la cola
            corte, hacia_abajo, lista_puntos = cola.get()  # Se obtienen los puntos que se deben analizar en este corte
            diccionario_secciones_a_marcar = {}

            # Para cada uno de los puntos se mira si hay una región que no sea la 0
            # Si no la hay se agrega, ya que será una región que se debe agregar a la máscara binaria
            for i, j in lista_puntos:
                if etiquetas[corte][i, j] != 0:
                    if etiquetas[corte][i, j] not in diccionario_secciones_a_marcar:
                        diccionario_secciones_a_marcar[etiquetas[corte][i, j]] = True

            # Se agregan a la máscara binaria las regiones que pertenecen al corte y al punto de interés
            for section in diccionario_secciones_a_marcar.keys():
                mascara_binaria_corte = mascara_binaria[:, :, corte]
                mascara_binaria_corte[etiquetas[corte] == section] = True
                mascara_binaria[:, :, corte] = mascara_binaria_corte

            # Se calculan los puntos no nulos y se transfieren a los cortes superiores o inferiores que correspondan
            puntos = np.where(mascara_binaria[:, :, corte])
            if corte > 0 and hacia_abajo:
                cola.put((corte - 1, True, tuple(zip(*puntos))))
            if corte < len(self.__cortes) - 1 and not hacia_abajo:
                cola.put((corte + 1, False, tuple(zip(*puntos))))

        if self._isRunning:
            binary_list = []
            color_list = []
            for c in range(len(self.__cortes)):
                binary = mascara_binaria[:, :, c]
                binary_list.append(binary)
                color_list.append(self.convertir_a_color(binary, self.__color))
            self.__modelo.set_segmentation(binary_list, color_list)
        self.tiempo.emit(time.monotonic()-inicio)
        self.finished.emit()

    def convertir_a_color(self, mascara_binaria, color):
        height, width = self.__cortes[0].shape
        color_image = np.zeros((height, width, 3), dtype=np.uint8)
        for j, c in enumerate(color):
            region_color = color_image[:, :, j]
            region_color[mascara_binaria] = c
            color_image[:, :, j] = region_color
        return color_image
