import pandas as pd
from PyQt5.QtCore import QThread
from PyQt5.QtWidgets import QFileDialog

from controller.componentes_conexas import ComponentesConexas
from controller.segmentador import Segmentador
from controller.umbralizador import Umbralizador
from model.dicom_model import DicomModel
from view.main_window import MainWindow
from typing import Optional


class Controller:

    def __init__(self, vista: MainWindow) -> None:
        super().__init__()
        self.__vista = vista
        self.__set_triggers_vista()
        self.__modelo: Optional[DicomModel] = None
        # Para activar o desactivar el click de la pantalla
        self.__click = False
        # Para la segmentación en paralelo para no bloquear el programa
        self.__thread: Optional[QThread] = None
        self.__worker = None
        self.__segmentation_color = [0, 172, 255]  # RGB
        self.__modos_segmentation = ['Isocontorno', 'Umbralización', 'Componentes conexas']
        self.__current_segmentation = 0
        # Para mostrar o no mostrar la segmentación
        self.__ver_no_ver_seg = self.__vista.get_estado_si_mostrar_segmentation()

    def __set_triggers_vista(self):
        """
        Realiza la asociación de los eventos de la vista con los métodos del controlador
        :return:
        """
        self.__vista.set_triggers(openfolder=self.__open_folder,
                                  selector_de_cortes=self.__selector_de_cortes,
                                  zoom_in=self.__zoom_in,
                                  zoom_out=self.__zoom_out,
                                  reset_zoom=self.__reiniciar_zoom,
                                  mostrar_dicom_info=self.__mostrar_dicom_data,
                                  mostrar_minimum_maximo=self.__mostrar_dialogo_minimum_maximo,
                                  on_click_image=self.__on_click_imagen,
                                  mostrar_histograma=self.__mostrar_histograma,
                                  ver_no_ver_seg=self.__actualizar_vista,
                                  mostrar_segmentation_dialog=self.__mostrar_dialogo_segmentation,
                                  cancelar_hilo=self.__apagar_thread,
                                  opacity_dialog=self.__mostrar_opacidad)

    def __selector_de_cortes(self, valor):
        """
        Método que se encarga de cambiar el corte según el valor del slider.
        :param valor: Corte seleccionado 1-n y lo convierte a 0-n-1
        :return:
        """
        self.__modelo.set_corte_actual(valor-1)
        self.__vista.corte_cambiado(valor, self.__modelo.get_numero_de_cortes())
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))

    def __open_folder(self):
        """
        Se encarga de mostrar el dialogo para abrir una carpeta con ficheros DICOM
        :return:
        """
        directorio = QFileDialog.getExistingDirectory(self.__vista, "Seleccione una carpeta con ficheros DICOM")
        try:
            self.__modelo = DicomModel(directorio)
            self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))
            self.__vista.configurar_selector_cortes(1, self.__modelo.get_numero_de_cortes())
            self.__vista.corte_cambiado(1, self.__modelo.get_numero_de_cortes())
            self.__vista.activar_acciones()
            self.__click = True
        except ValueError as err:
            self.__vista.mostrar_mensaje_error(err.args[0])

    # ------------------------------------
    # Zoom utilities
    # ------------------------------------

    def __zoom_in(self):
        """
        Se encarga de ampliar la imagen
        :return:
        """
        self.__modelo.incrementar_zoom()
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))

    def __zoom_out(self):
        """
        Se encarga de reducir la imagen
        :return:
        """
        self.__modelo.decrementar_zoom()
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))

    def __reiniciar_zoom(self):
        """Se encarga de mostrar la imagen a tamaño natural"""
        self.__modelo.reiniciar_zoom()
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))

    # ------------------------------
    # Dicom Data
    # ------------------------------

    def __mostrar_dicom_data(self):
        """
        Proceso que se encarga de obtener la información necesaria para mostrar una tabla con la información
        del corte DICOM
        :return:
        """
        keys, significado,  valores = self.__modelo.get_processed_info_corte_actual()
        data_pd = pd.DataFrame({'Llaves': keys, 'Significado': significado, 'Valores': valores})
        self.__vista.mostrar_formulario_tabla(data_pd, self.__save_dicom_data)

    def __save_dicom_data(self):
        """
        Permite guardar la información mostrada en un fichero CSV - JSON o Excel.
        :return:
        """
        keys, significado, valores = self.__modelo.get_processed_info_corte_actual()
        data_pd = pd.DataFrame({'Llaves': keys, 'Significado': significado, 'Valores': valores})
        filter_types = "CSV (*.csv);;Microsoft Excel (*.xlsx);;JSON (*.json)"
        file = QFileDialog.getSaveFileName(self.__vista, "Guardar cabecera", ".", filter_types)
        self.__save_pandas(data_pd, file[0], file[1])

    @staticmethod
    def __save_pandas(data: pd.DataFrame, file_name, tipo):
        """
        Método encargado de guardar realmente un pandas
        :param data: DataFrame de pandas con la información que se quiere guardar
        :param file_name: Nombre del fichero
        :param tipo: Extensión de destino
        :return:
        """
        if tipo == "CSV (*.csv)":
            if file_name.endswith('.csv'):
                data.to_csv(file_name)
            else:
                data.to_csv(file_name+'.csv')
        elif tipo == "Microsoft Excel (*.xlsx)":
            if file_name.endswith('.xlsx'):
                with pd.ExcelWriter(file_name) as writer:
                    data.to_excel(writer)
            else:
                with pd.ExcelWriter(file_name+'.xlsx') as writer:
                    data.to_excel(writer)
        elif tipo == "JSON (*.json)":
            if file_name.endswith('.json'):
                data.to_json(file_name)
            else:
                data.to_json(file_name+'.json')

    # --------------------------------------
    # Configurar valor máximo y mínimo de visualización
    # --------------------------------------

    def __mostrar_dialogo_minimum_maximo(self):
        """
        Se encarga de preparar el dialogo para poder realizar el cambio de valores
        :return:
        """
        global_minimum, global_maximum = self.__modelo.get_global_minimum_maximo()
        valor_minimum, valor_maximum = self.__modelo.get_valor_minimum_maximo_de_visualization()
        self.__vista.mostrar_formulario_minimum_maximo(global_minimum, global_maximum, valor_minimum, valor_maximum,
                                                       self.__cambiar_minimum_maximum)

    def __cambiar_minimum_maximum(self, valor_minimum, valor_maximum):
        """
        Se encarga de aplicar el proceso de cambio de visualización
        :param valor_minimum: valor mínimo deseado
        :param valor_maximum: valor máximo deseado
        :return:
        """
        try:
            self.__modelo.set_minimum_maximo_visualization(valor_minimum, valor_maximum)
            self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))
        except ValueError as e:
            self.__vista.mostrar_mensaje_error(e.args[0])

    # --------------------------------------
    # Click Imagen
    # --------------------------------------

    def __on_click_imagen(self, x, y):
        """
        Método que muestra los valores de un pixel concreto de la imagen
        :param x: Coordenada X
        :param y: Coordenada Y
        :return:
        """
        if self.__click:
            if self.__modelo is not None and self.__vista.get_estado_si_realizar_segmentation():
                self.__segmentar(x, y)

            if self.__modelo is not None:
                valor_rgb = self.__modelo.get_corte_actual_rgb(mostrar_segmentation=False)[y, x, 0]
                valor = self.__modelo.get_corte_actual_pixels()[y, x]
                self.__vista.modificar_valores(x, y, valor, valor_rgb)

    # -------------------------------------
    # Segmentación
    # -------------------------------------

    def __segmentar(self, x, y):
        # Se desactivan las acciones
        self.__vista.desactivar_acciones()
        self.__vista.activar_button_cancelar()
        self.__click = False
        # Se crea el hilo
        self.__thread = QThread()
        if self.__modos_segmentation[self.__current_segmentation] == 'Isocontorno':
            self.__worker = Segmentador(self.__modelo, x, y, color=self.__segmentation_color, parent=None)
            self.__vista.configurar_barra_progreso(0, 0)
            self.__worker.tiempo.connect(self.__actualizar_tiempo)
        elif self.__modos_segmentation[self.__current_segmentation] == 'Umbralización':
            self.__worker = Umbralizador(self.__modelo, x, y, color=self.__segmentation_color, parent=None)
            self.__vista.configurar_barra_progreso(0, self.__modelo.get_numero_de_cortes())
            self.__worker.progress.connect(self.__actualizar_barra_progreso)
            self.__worker.tiempo.connect(self.__actualizar_tiempo)
        elif self.__modos_segmentation[self.__current_segmentation] == 'Componentes conexas':
            self.__worker = ComponentesConexas(self.__modelo, x, y, color=self.__segmentation_color, parent=None)
            self.__vista.configurar_barra_progreso(0, 0)
            self.__worker.tiempo.connect(self.__actualizar_tiempo)
        self.__worker.moveToThread(self.__thread)

        # Se asocian las acciones
        self.__thread.started.connect(self.__worker.run)
        self.__worker.finished.connect(self.__thread.quit)
        self.__worker.finished.connect(self.__worker.deleteLater)
        self.__thread.finished.connect(self.__thread.deleteLater)
        self.__thread.finished.connect(self.__activar_acciones)
        self.__thread.finished.connect(self.__mostrar_imagen)
        self.__thread.start()

    def __actualizar_tiempo(self, tiempo):
        self.__vista.modificar_tiempo("{} s".format(round(tiempo, 4)))

    def __activar_acciones(self):
        self.__vista.activar_acciones()
        self.__vista.desactivar_button_cancelar()
        self.__vista.reiniciar_barra_progreso()
        self.__click = True

    def __actualizar_barra_progreso(self, i):
        self.__vista.actualizar_progreso(i)

    def __apagar_thread(self):
        if self.__worker is not None:
            self.__worker.stop()
            self.__thread.quit()
            self.__thread.wait()

    def __mostrar_imagen(self):
        self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))

    def __actualizar_vista(self, valor):
        self.__ver_no_ver_seg = valor
        self.__ver_no_ver_seg = self.__vista.get_estado_si_mostrar_segmentation()
        self.__mostrar_imagen()

    def __mostrar_dialogo_segmentation(self):
        """
        Se encarga de preparar el dialogo para poder ajustar los parámetros de la segmentación
        :return:
        """
        self.__vista.mostrar_formulario_segmentation(self.__configurar_segmentation, self.__modos_segmentation,
                                                     self.__current_segmentation, self.__segmentation_color)

    def __configurar_segmentation(self, idx_modo, color):
        self.__current_segmentation = idx_modo
        self.__segmentation_color = color

    # -------------------------------------
    # Histograma
    # -------------------------------------

    def __mostrar_histograma(self):
        """
        Se encarga de mostrar el histograma de la imagen
        :return:
        """
        histograma, rango = self.__modelo.get_histogram_pixels(bins=16)
        self.__vista.mostrar_histograma(histograma, rango, self.__calcular_histograma)

    def __calcular_histograma(self, bins):
        """
        Se encarga de calcular el histograma con los bins indicados
        :param bins:
        :return:
        """
        histograma, _ = self.__modelo.get_histogram_pixels(bins=bins)
        return histograma

    # ---------------------------------
    # Opacidad
    # ---------------------------------

    def __mostrar_opacidad(self):
        self.__vista.mostrar_dialogo_opacidad(self.__modelo.get_valor_opacidad(), self.__configurar_opacidad)

    def __configurar_opacidad(self, valor: float):
        try:
            self.__modelo.set_valor_opacidad(valor)
            self.__vista.set_imagen(self.__modelo.get_corte_actual_rgb(mostrar_segmentation=self.__ver_no_ver_seg))
        except ValueError as e:
            self.__vista.mostrar_mensaje_error(e.args[0])
