import time
from queue import Queue

import numpy as np
from PyQt5.QtCore import QObject, pyqtSignal

from model.dicom_model import DicomModel


class Segmentador(QObject):

    finished = pyqtSignal()
    progress = pyqtSignal(int)
    tiempo = pyqtSignal(float)

    def __init__(self, modelo: DicomModel, x, y, color, parent: None) -> None:
        super().__init__(parent)
        self.__modelo = modelo
        self.__cortes = modelo.get_all_cortes_pixels()
        self.__x = x
        self.__y = y
        self.__corte = self.__modelo.get_index_corte_actual()

        self.__valor = self.__cortes[self.__corte][y, x]
        self.__color = color
        self._isRunning = True

    def stop(self):
        self._isRunning = False

    def run(self):
        inicio = time.monotonic()
        height, width = self.__cortes[0].shape
        visitados = np.zeros((height, width, len(self.__cortes)), dtype=np.bool)
        mascara_binaria = np.zeros((height, width, len(self.__cortes)), dtype=np.bool)
        cola = Queue()
        cola.put((self.__y, self.__x, self.__corte))
        while not cola.empty() and self._isRunning:
            i, j, canal = cola.get()
            visitados[i, j, canal] = True
            if self.__cortes[canal][i, j] >= self.__valor:
                mascara_binaria[i, j, canal] = True
                combinaciones = self.generar_combinaciones(i, j, canal)
                for new_i, new_j, new_c in combinaciones:
                    if not visitados[new_i, new_j, new_c] and self.__cortes[new_c][new_i, new_j] >= self.__valor:
                        cola.put((new_i, new_j, new_c))
                        visitados[new_i, new_j, new_c] = True
        if self._isRunning:
            binary_list = []
            color_list = []
            for c in range(len(self.__cortes)):
                binary = mascara_binaria[:, :, c]
                binary_list.append(binary)
                color_list.append(self.convertir_a_color(binary, self.__color))
            self.__modelo.set_segmentation(binary_list, color_list)

        self.tiempo.emit(time.monotonic() - inicio)
        self.finished.emit()

    def generar_combinaciones(self, i, j, canal):
        height, width = self.__cortes[0].shape
        canales = len(self.__cortes)
        combinaciones = []
        if i+1 < height:
            combinaciones.append((i+1, j, canal))
        if i-1 > 0:
            combinaciones.append((i - 1, j, canal))
        if j+1 < width:
            combinaciones.append((i, j+1, canal))
        if j-1 > 0:
            combinaciones.append((i, j-1, canal))
        if canal+1 < canales:
            combinaciones.append((i, j, canal+1))
        if canal-1 > 0:
            combinaciones.append((i, j, canal - 1))

        return combinaciones

    def convertir_a_color(self, mascara_binaria, color):
        height, width = self.__cortes[0].shape
        color_image = np.zeros((height, width, 3), dtype=np.uint8)
        for j, c in enumerate(color):
            region_color = color_image[:, :, j]
            region_color[mascara_binaria] = c
            color_image[:, :, j] = region_color
        return color_image
