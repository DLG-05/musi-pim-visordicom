import time

import numpy as np
from PyQt5.QtCore import QObject, pyqtSignal

from model.dicom_model import DicomModel


class Umbralizador(QObject):

    finished = pyqtSignal()
    progress = pyqtSignal(int)
    tiempo = pyqtSignal(float)

    def __init__(self, modelo: DicomModel, x, y, color, parent=None):
        super().__init__(parent)
        self.__modelo = modelo
        self.__cortes = modelo.get_all_cortes_pixels()
        self.__x = x
        self.__y = y
        self.__corte = self.__modelo.get_index_corte_actual()
        self.__valor = self.__cortes[self.__corte][y, x]
        self.__color = color
        self._isRunning = True

    def stop(self):
        self._isRunning = False

    def run(self):
        inicio = time.monotonic()
        binary_masks = []
        color_masks = []
        for i, imagen in enumerate(self.__cortes):
            binary = imagen >= self.__valor
            height, width = imagen.shape
            color_image = np.zeros((height, width, 3), dtype=np.uint8)
            for j, c in enumerate(self.__color):
                region_color = color_image[:, :, j]
                region_color[binary] = c
                color_image[:, :, j] = region_color
            binary_masks.append(binary)
            color_masks.append(color_image)
            self.progress.emit(i+1)
            if not self._isRunning:
                break
        if self._isRunning:
            self.__modelo.set_segmentation(binary_masks, color_masks)
        self.tiempo.emit(time.monotonic() - inicio)
        self.finished.emit()
