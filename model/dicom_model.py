import cv2
import magic
import numpy as np
import re
import os
from pydicom import dcmread


class DicomModel:

    ZOOM_MAX = 3
    ZOOM_PASS = 0.1
    ZOOM_MIN = 0.1
    ERROR_MIN_MAX = "El valor mínimo no debe ser más grande que el máximo, se mantienen los valores anteriores"

    def __init__(self, path: str) -> None:
        self.__mime = magic.Magic(mime=True)
        self.__path = path
        self.__dicom_files = []
        self.__corte_actual = 0
        self.__zoom_factor = 1.0
        self.__binary_segmentation = None
        self.__color_segmentation = None

        self.__global_minimum = 0
        self.__global_maximum = 255
        self.__valor_minimum = self.__global_minimum
        self.__valor_maximo = self.__global_maximum

        self.__valor_opacidad = 0.2

        self.__load()

    # -----------------------------------------------------------------------
    # Getters
    # -----------------------------------------------------------------------

    def get_corte_actual(self):
        return self.__dicom_files[self.__corte_actual]

    def get_index_corte_actual(self):
        return self.__corte_actual

    def get_corte_actual_pixels(self):
        img = cv2.resize(self.__dicom_files[self.__corte_actual].pixel_array, None,
                         fx=self.__zoom_factor, fy=self.__zoom_factor)
        img = np.clip(img, self.__valor_minimum, self.__valor_maximo)
        return img

    def get_all_cortes_pixels(self):
        lista = []
        for corte in self.__dicom_files:
            img = corte.pixel_array
            img = cv2.resize(img, None, fx=self.__zoom_factor, fy=self.__zoom_factor)
            img = np.clip(img, self.__valor_minimum, self.__valor_maximo)
            lista.append(img)
        return lista

    def get_corte_actual_rgb(self, mostrar_segmentation=True):
        img = self.get_corte_actual_pixels()
        img_gray_scale = self.escalar_imagen(img)
        img_color = cv2.cvtColor(img_gray_scale, cv2.COLOR_GRAY2RGB)

        if self.__binary_segmentation is not None and mostrar_segmentation:
            color_seg = self.__color_segmentation[self.__corte_actual]
            height, width = img_gray_scale.shape
            color_seg_scaled = cv2.resize(color_seg, (width, height))
            dst = cv2.addWeighted(img_color, 1-self.__valor_opacidad, color_seg_scaled, self.__valor_opacidad, 0)
            return dst
        else:
            return img_color

    def get_histogram_pixels(self, bins=16):
        imagen = self.get_corte_actual_pixels()
        maximo = np.max(imagen)
        minimum = np.min(imagen)
        hist = np.histogram(self.get_corte_actual_pixels(), bins=bins)
        return hist, maximo-minimum

    def get_numero_de_cortes(self):
        return len(self.__dicom_files)

    def get_info_corte_actual(self):
        return list(self.__dicom_files[self.__corte_actual].items())

    def get_processed_info_corte_actual(self):
        """
        Se encarga de mostrar la información dicom mediante la conversión de su str adaptada
        :return:
        """
        data = self.get_corte_actual()
        lines = str(data).split("\n")
        keys = []
        significado = []
        valores = []
        for i, line in enumerate(lines):
            if i != 0 and "----" not in line and "" != line:
                # https://pythex.org/
                matcher = re.search(r'(\([0-9]*,\s[0-9]+\))\s([a-zA-Z ()\[\]]*)[\s]{2,1000}(.*)', line,  re.M | re.I)
                if matcher:
                    keys.append(matcher.group(1))
                    significado.append(matcher.group(2))
                    valores.append(matcher.group(3))

        return keys, significado, valores

    def get_valor_minimum_maximo_de_visualization(self):
        return self.__valor_minimum, self.__valor_maximo

    def get_global_minimum_maximo(self):
        return self.__global_minimum, self.__global_maximum

    def get_valor_opacidad(self):
        return self.__valor_opacidad

    # -----------------------------------------------------------------------
    # Setters
    # -----------------------------------------------------------------------

    def set_corte_actual(self, i: int):
        if self.get_numero_de_cortes() < i < 0:
            raise Exception("DicomModel - Corte indicado inválido")
        self.__corte_actual = i

    def incrementar_zoom(self):
        if self.__zoom_factor < self.ZOOM_MAX:
            self.__zoom_factor += self.ZOOM_PASS

    def decrementar_zoom(self):
        if self.__zoom_factor > self.ZOOM_MIN:
            self.__zoom_factor -= self.ZOOM_PASS

    def reiniciar_zoom(self):
        self.__zoom_factor = 1.0

    def set_segmentation(self, binary_masks, color_masks):
        self.__binary_segmentation = binary_masks
        self.__color_segmentation = color_masks

    def set_minimum_maximo_visualization(self, minimum, maximo):
        if minimum > maximo:
            raise ValueError(self.ERROR_MIN_MAX)
        if minimum < self.__global_minimum:
            raise ValueError("El mínimo no puede ser más pequeño que el mínimo global")
        if maximo > self.__global_maximum:
            raise ValueError("El máximo no puede ser más grande que el máximo global")

        self.__valor_minimum = minimum
        self.__valor_maximo = maximo

    def set_valor_opacidad(self, valor: float):
        if valor < 0 or valor > 1:
            raise ValueError("El valor debe ser real y entre 0 y 1")
        self.__valor_opacidad = valor

    # -----------------------------------------------------------------------
    # Load functions
    # -----------------------------------------------------------------------

    def __load(self):
        if os.path.isdir(self.__path):
            self.__load_from_path()
        elif os.path.isfile(self.__path) and self.__mime.from_file(self.__path) == 'application/dicom':
            self.__dicom_files.append(dcmread(self.__path))
        else:
            raise ValueError("Not Dicom Detected")

    def __load_from_path(self):
        for img in os.listdir(self.__path):
            if os.path.isfile(os.path.join(self.__path, img)):
                if self.__mime.from_file(os.path.join(self.__path, img)) != 'application/dicom':
                    raise ValueError("Not DICOM File")
                else:
                    dicom_file = dcmread(os.path.join(self.__path, img))
                    self.__dicom_files.append(dicom_file)
                    minimum = np.min(dicom_file.pixel_array)
                    maximo = np.max(dicom_file.pixel_array)
                    if minimum < self.__global_minimum:
                        self.__global_minimum = minimum
                    if maximo > self.__global_maximum:
                        self.__global_maximum = maximo
            elif os.path.isdir(os.path.join(self.__path, img)):
                raise ValueError("Not DICOM File")
        self.__valor_minimum = self.__global_minimum
        self.__valor_maximo = self.__global_maximum

    # -----------------------------------------------------------------------
    # Utilidades
    # -----------------------------------------------------------------------
    def escalar_imagen(self, img):
        img_scaled = img.copy()
        img_scaled = ((img_scaled - self.__valor_minimum) / (self.__valor_maximo-self.__valor_minimum)) * 255
        return img_scaled.astype(np.uint8)
