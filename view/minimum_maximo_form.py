from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi


class MinimumMaximoDialog(QDialog):

    def __init__(self, global_minimum, global_maximo, valor_minimum, valor_maximum, apply_function,
                 parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/Visualization_Minimum_Maximum.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__apply_function = apply_function

        self.__valor_minimum = valor_minimum
        self.__valor_maximum = valor_maximum
        self.__global_minimum = global_minimum
        self.__global_maximum = global_maximo

        self.minimumSpinBox.setMinimum(global_minimum)
        self.minimumSpinBox.setMaximum(global_maximo)
        self.minimumSpinBox.setValue(valor_minimum)
        self.maximoSpinBox.setMinimum(global_minimum)
        self.maximoSpinBox.setMaximum(global_maximo)
        self.maximoSpinBox.setValue(valor_maximum)
        self.resetMaximoButton.clicked.connect(self.reset_maximum)
        self.resetMinimumButton.clicked.connect(self.reset_minimum)
        self.minimumButton.clicked.connect(self.goto_minimum)
        self.maximoButton.clicked.connect(self.goto_maximo)

    def reset_minimum(self):
        self.minimumSpinBox.setValue(self.__valor_minimum)

    def reset_maximum(self):
        self.maximoSpinBox.setValue(self.__valor_maximum)

    def goto_maximo(self):
        self.maximoSpinBox.setValue(self.__global_maximum)

    def goto_minimum(self):
        self.minimumSpinBox.setValue(self.__global_minimum)

    def accept(self) -> None:
        maximo = self.maximoSpinBox.value()
        minimum = self.minimumSpinBox.value()
        self.__apply_function(minimum, maximo)
        super().accept()
