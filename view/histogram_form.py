import numpy as np
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget
from PyQt5.uic import loadUi

from view.ui_components.matplotlibcanvas import MatplotlibCanvas


class HistogramForm(QWidget):

    def __init__(self, histogram, rango, recalcular_histograma, parent=None) -> None:
        """
        Ventana para mostrar los histogramas
        :param histogram: histograma base
        :param rango: rango de trabajo
        :param recalcular_histograma: función de modificación del gráfico
        :param parent:
        """
        super().__init__(parent)
        loadUi("./view/ui_files/Histogram_Dialog.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__valores = None
        self.__histogram = histogram
        self.__rango = rango
        self.__canvas = MatplotlibCanvas()
        self.__recalcular_histograma = recalcular_histograma
        self.__canvas.axes.bar(histogram[1][1:], histogram[0], width=100)
        self.layout.replaceWidget(self.remplazar, self.__canvas)
        self.__asignar_bins()
        self.nBinsComboBox.currentIndexChanged.connect(self.__on_change_value)

    def __asignar_bins(self):
        """
        Asigna los bins posibles para realizar los cambios en el histograma
        Solamente se mostrarán 15 posibilidades + la que utiliza por defecto
        el modelo de datos
        :return:
        """
        self.__valores = list(np.linspace(2, self.__rango, 15, dtype=np.int))
        if len(self.__histogram[0]) not in self.__valores:
            self.__valores.append(len(self.__histogram[0]))
            self.__valores.sort()
        valores = ["{}".format(x) for x in self.__valores]
        self.nBinsComboBox.addItems(valores)
        i = 0
        acabar = False
        objetivo = len(self.__histogram[0])
        while not acabar and i < len(self.__valores):
            if self.__valores[i] == objetivo:
                acabar = True
            else:
                i += 1
        self.nBinsComboBox.setCurrentIndex(i)

    def __on_change_value(self, value):
        value = self.__valores[value]
        self.__histogram = self.__recalcular_histograma(value)
        old_canvas = self.__canvas
        self.__canvas = MatplotlibCanvas()
        self.__canvas.axes.bar(self.__histogram[1][1:], self.__histogram[0], width=100)
        self.layout.replaceWidget(old_canvas, self.__canvas)
        old_canvas.deleteLater()
