from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QTableView

from view.ui_components.tablepandas import TablePandas


class TableForm(QWidget):

    def __init__(self, data, save_function, parent=None) -> None:
        """
        Dialogo para mostrar la información del detallada del dicom
        :param data:
        :param save_function:
        :param parent:
        """
        super().__init__(parent)
        self.__data = data
        # Se asocia un layout con la tabla y el botón de guardar
        layout = QVBoxLayout()
        table_model = TablePandas(data)
        tableview = QTableView()
        tableview.setModel(table_model)
        tableview.resizeColumnsToContents()
        layout.addWidget(tableview)
        save_table_data_button = QPushButton("Guardar")
        layout.addWidget(save_table_data_button)
        self.setLayout(layout)
        self.setWindowTitle("Información DICOM")
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        save_table_data_button.clicked.connect(save_function)
        self.resize(600, 300)
