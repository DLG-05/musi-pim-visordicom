from PyQt5 import QtGui
from PyQt5.QtGui import QPalette, QImage, QPixmap
from PyQt5.QtWidgets import QLabel, QSizePolicy


class ImageLabel(QLabel):

    def __init__(self, on_click=None, parent=None) -> None:
        """
        Encargada de definir una clase para mostrar imágenes mediante la GUI
        :param on_click: La acción que ocurrirá cuando se haga click
        :param parent:
        """
        super().__init__(parent)
        self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.setBackgroundRole(QPalette.Base)
        self.setScaledContents(True)
        self.__on_click = on_click

    def set_image(self, img_array):
        """
        Realiza la asociación de la imagen mediante un array de pixels en RGB
        :param img_array:
        :return:
        """
        image = QImage(img_array, img_array.shape[1], img_array.shape[0],
                       img_array.shape[1] * 3, QImage.Format_RGB888)
        pix = QPixmap(image)
        self.setPixmap(pix)
        self.resize(pix.width(), pix.height())

    def mousePressEvent(self, ev: QtGui.QMouseEvent) -> None:
        """
        Evento cuando se pulsa la foto
        :param ev:
        :return:
        """
        if self.__on_click is not None:
            position = ev.pos()
            x = position.x()
            y = position.y()
            self.__on_click(x, y)
        super().mousePressEvent(ev)
