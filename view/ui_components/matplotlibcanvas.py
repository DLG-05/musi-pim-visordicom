from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib import pyplot as plt


class MatplotlibCanvas(FigureCanvasQTAgg):

    def __init__(self, **kwargs):
        """
        Clase que permite mostrar gráficas de Matplotlib en PyQT
        :param kwargs:
        """
        figure, axes = plt.subplots(**kwargs)
        self.figure = figure
        self.axes: plt.Axes = axes
        super().__init__(figure)
