import pandas as pd
from PyQt5.QtGui import QIcon, QCursor
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QToolTip
from PyQt5.uic import loadUi

from view.minimum_maximo_form import MinimumMaximoDialog
from view.histogram_form import HistogramForm
from view.opacity_form import OpacityDialog
from view.segmentation_form import SegmentationDialog
from view.table_form import TableForm
from view.ui_components.imagelabel import ImageLabel


class MainWindow(QMainWindow):

    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        loadUi("./view/ui_files/Main_Window.ui", self)
        self.setWindowIcon(QIcon('./view/ui_files/main_icon.png'))
        self.__img = None
        self.__selector_de_cortes_function = None
        self.__formulario_tabla = None
        self.__formulario_minimum_maximo = None
        self.__formulario_histograma = None
        self.__formulario_segmentation = None
        self.__formulario_opacidad = None
        self.__on_click_image = None

    def set_triggers(self, openfolder, selector_de_cortes, zoom_in, zoom_out, reset_zoom, mostrar_dicom_info,
                     mostrar_minimum_maximo, on_click_image, mostrar_histograma, ver_no_ver_seg,
                     mostrar_segmentation_dialog, cancelar_hilo, opacity_dialog):
        """
        Realiza la asociación de todas las funciones de la interfaz
        :param openfolder: Abrir una carpeta
        :param selector_de_cortes: acción para el slider
        :param zoom_in: zoom
        :param zoom_out: zoom
        :param reset_zoom: zoom
        :param mostrar_dicom_info: Acción al pulsar el botón para mostrar la información del dicom
        :param mostrar_minimum_maximo: Acción para mostrar el dialogo de brillo-contraste
        :param on_click_image: Acción al pulsar sobre la imagen
        :param mostrar_histograma: Acción para abrir el dialogo para ver el histograma
        :param ver_no_ver_seg: Permite mostrar o no la segmentación realizada si hay alguna
        :param mostrar_segmentation_dialog: Permite mostrar el dialogo de configuración para la segmentación.
        :param cancelar_hilo: para la ejecución de la segmentación
        :param opacity_dialog: muestra el diálogo para configurar la opacidad de la segmentación
        :return:
        """
        self.actionAbrir_Carpeta.triggered.connect(openfolder)
        self.__selector_de_cortes_function = selector_de_cortes
        self.selectorDeCortesSlicer.valueChanged.connect(self.__tooltip_slider)
        self.actionZoom_In.triggered.connect(zoom_in)
        self.actionZoom_In.setShortcut('Ctrl++')
        self.actionZoom_Out.triggered.connect(zoom_out)
        self.actionZoom_Out.setShortcut('Ctrl+-')
        self.actionReset_Zoom.triggered.connect(reset_zoom)
        self.actionReset_Zoom.setShortcut('Ctrl+0')
        self.selectorDeCortesSlicer.sliderMoved.connect(self.__tooltip_slider)
        self.actionInfo_del_DICOM.triggered.connect(mostrar_dicom_info)
        self.actionContrastar_Imagen.triggered.connect(mostrar_minimum_maximo)
        self.actionHistograma.triggered.connect(mostrar_histograma)
        self.__on_click_image = on_click_image
        self.actionMostrar_Segmentacion.triggered.connect(ver_no_ver_seg)
        self.actionConfigurar_Segmentacion.triggered.connect(mostrar_segmentation_dialog)
        self.cancelButton.clicked.connect(cancelar_hilo)
        self.actionGrado_de_la_mascara.triggered.connect(opacity_dialog)

    def __tooltip_slider(self, value):
        """
        Agrega un popup al pasar el ratón sobre el slider
        :param value: valor que se debe mostrar
        :return:
        """
        QToolTip.showText(QCursor.pos(), str(value), self)
        self.__selector_de_cortes_function(value)

    def set_imagen(self, imagen):
        """
        Permite mostrar una imagen en la área principal
        :param imagen: Imagen en formato RGB
        :return:
        """
        if self.__img is not None:
            self.__img.deleteLater()
        self.__img = ImageLabel(self.__on_click_image)
        self.__img.set_image(imagen)
        self.imageScrollArea.setWidget(self.__img)

    def configurar_selector_cortes(self, minimum, maximo):
        """
        Configura los rangos del selector de cortes
        :param minimum: Corte mínimo
        :param maximo: Corte máximo
        :return:
        """
        self.selectorDeCortesSlicer.setEnabled(True)
        self.selectorDeCortesSlicer.setMinimum(minimum)
        self.selectorDeCortesSlicer.setMaximum(maximo)

    def corte_cambiado(self, i, rango):
        """
        Modifica el valor de la etiqueta que muestra el corte
        :param i: corte seleccionado
        :param rango: valor máximo
        :return:
        """
        self.selectorDeCortesValue.setText("{}/{}".format(i, rango))

    def mostrar_mensaje_error(self, mensaje):
        """
        Dialogo para indicar un mensaje de error de manera gráfica
        :param mensaje: Mensaje para mostrar
        :return:
        """
        error = QMessageBox(self)
        error.setText("Error")
        error.setInformativeText(mensaje)
        error.setIcon(QMessageBox.Warning)
        error.setWindowTitle("Error")
        error.exec_()

    def activar_acciones(self):
        """
        Activa todos los botones
        :return:
        """
        self.actionInfo_del_DICOM.setEnabled(True)
        self.actionZoom_In.setEnabled(True)
        self.actionZoom_Out.setEnabled(True)
        self.actionReset_Zoom.setEnabled(True)
        self.actionContrastar_Imagen.setEnabled(True)
        self.actionHistograma.setEnabled(True)
        self.actionActivar_Segmentacion.setEnabled(True)
        self.actionAbrir_Carpeta.setEnabled(True)
        self.selectorDeCortesSlicer.setEnabled(True)
        self.actionMostrar_Segmentacion.setEnabled(True)
        self.actionConfigurar_Segmentacion.setEnabled(True)
        self.actionGrado_de_la_mascara.setEnabled(True)

    def desactivar_acciones(self):
        """
        Desactiva todos los botones
        :return:
        """
        self.actionInfo_del_DICOM.setEnabled(False)
        self.actionZoom_In.setEnabled(False)
        self.actionZoom_Out.setEnabled(False)
        self.actionReset_Zoom.setEnabled(False)
        self.actionContrastar_Imagen.setEnabled(False)
        self.actionHistograma.setEnabled(False)
        self.actionActivar_Segmentacion.setEnabled(False)
        self.actionAbrir_Carpeta.setEnabled(False)
        self.selectorDeCortesSlicer.setEnabled(False)
        self.actionMostrar_Segmentacion.setEnabled(False)
        self.actionConfigurar_Segmentacion.setEnabled(False)
        self.actionGrado_de_la_mascara.setEnabled(False)

    def get_estado_si_realizar_segmentation(self):
        return self.actionActivar_Segmentacion.isChecked()

    def get_estado_si_mostrar_segmentation(self):
        return self.actionMostrar_Segmentacion.isChecked()

    def mostrar_formulario_minimum_maximo(self, global_minimum, global_maximum, minimum_value, maximum_value,
                                          return_function):
        self.__formulario_minimum_maximo = MinimumMaximoDialog(global_minimum=global_minimum,
                                                               global_maximo=global_maximum,
                                                               valor_minimum=minimum_value,
                                                               valor_maximum=maximum_value,
                                                               apply_function=return_function,
                                                               parent=None)
        self.__formulario_minimum_maximo.show()

    def mostrar_formulario_tabla(self, data: pd.DataFrame, save_function):
        self.__formulario_tabla = TableForm(data, save_function)
        self.__formulario_tabla.show()

    def modificar_valores(self, x, y, valor, valor_rgb):
        """
        Se encarga de mostrar los valores de la imagen que se hayan seleccionado
        :param x: coordenada x
        :param y: coordenada y
        :param valor: valor sin convertir
        :param valor_rgb: valor convertido
        :return:
        """
        self.selectedPixelLabel.setText("({},{})".format(x, y))
        self.originalValueLabel.setText(str(valor))
        self.processedValueLabel.setText(str(valor_rgb))

    def modificar_tiempo(self, tiempo: str):
        self.tiempoLabel.setText(tiempo)

    def mostrar_histograma(self, histograma, rango, calcular_histograma):
        self.__formulario_histograma = HistogramForm(histograma, rango, calcular_histograma)
        self.__formulario_histograma.show()

    def mostrar_formulario_segmentation(self, callback, segmentation_modes, idx_segmentation_modes, color):
        self.__formulario_segmentation = SegmentationDialog(callback, segmentation_modes, idx_segmentation_modes, color)
        self.__formulario_segmentation.show()

    def configurar_barra_progreso(self, minimum, maximo):
        self.progressBar.setMaximum(maximo)
        self.progressBar.setMinimum(minimum)
        self.progressBar.setValue(0)

    def reiniciar_barra_progreso(self):
        self.progressBar.setMaximum(100)
        self.progressBar.setMinimum(0)
        self.progressBar.setValue(0)

    def actualizar_progreso(self, i):
        self.progressBar.setValue(i)

    def desactivar_button_cancelar(self):
        self.cancelButton.setEnabled(False)

    def activar_button_cancelar(self):
        self.cancelButton.setEnabled(True)

    def mostrar_dialogo_opacidad(self, valor, callback):
        self.__formulario_opacidad = OpacityDialog(valor, callback)
        self.__formulario_opacidad.show()
