import sys

from PyQt5.QtWidgets import QApplication

from controller.controller import Controller
from view.main_window import MainWindow

controller = None


def run():
    global controller
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    controller = Controller(window)

    sys.exit(app.exec())


if __name__ == '__main__':
    run()


